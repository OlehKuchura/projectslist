package com.example.projectslistapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;

import java.util.HashSet;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupToolbarWithNavController();
    }

    private void setupToolbarWithNavController() {
        Fragment host = getSupportFragmentManager().findFragmentById(R.id.host);
        if (host != null) {
            NavController navController = ((NavHostFragment) host).getNavController();
            HashSet<Integer> dest = new HashSet<>();
            dest.add(R.id.loginFragment);
            dest.add(R.id.projectsListFragment);
            AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(dest)
                    .build();
            NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        }
    }
}