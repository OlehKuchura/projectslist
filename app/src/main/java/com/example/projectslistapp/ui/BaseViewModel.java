package com.example.projectslistapp.ui;

import androidx.lifecycle.ViewModel;

import io.reactivex.disposables.Disposable;

public abstract class BaseViewModel extends ViewModel {
    protected Disposable disposable;

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.dispose();
        }
    }
}
