package com.example.projectslistapp.ui.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.example.projectslistapp.R;
import com.example.projectslistapp.databinding.FragmentLoginBinding;
import com.example.projectslistapp.ui.BaseFragment;
import com.example.projectslistapp.ui.Event;

public class LoginFragment extends BaseFragment<FragmentLoginBinding> {

    private LoginViewModel viewModel;

    @Override
    protected FragmentLoginBinding bindLayout(LayoutInflater inflater, ViewGroup parent) {
        return FragmentLoginBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewModel();
        observeOpenProjectsEvent();
        observeLoginErrorEvent();
    }

    private void setupViewModel() {
        viewModel = new ViewModelProvider(requireActivity(), new LoginViewModelFactory())
                .get(LoginViewModel.class);
        binding.setViewModel(viewModel);
        binding.executePendingBindings();
        viewModel.checkToken();
    }

    private void observeOpenProjectsEvent() {
        viewModel.getOpenProjectsEvent().observe(getViewLifecycleOwner(),
                new Event.EventObserver<>(content -> navigateToProjects()));
    }

    private void observeLoginErrorEvent() {
        viewModel.getLoginErrorEvent().observe(getViewLifecycleOwner(),
                new Event.EventObserver<>(content -> showError()));
    }

    private void navigateToProjects() {
        NavHostFragment.findNavController(this).navigate(
                LoginFragmentDirections.actionLoginFragmentToProjectsListFragment()
        );
    }

    private void showError() {
        Toast.makeText(requireContext(), R.string.login_error, Toast.LENGTH_SHORT).show();
    }
}
