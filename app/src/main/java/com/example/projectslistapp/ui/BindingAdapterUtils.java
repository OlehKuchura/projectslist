package com.example.projectslistapp.ui;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.example.projectslistapp.R;

public class BindingAdapterUtils {

    @BindingAdapter("app:imageURL")
    public static void loadImage(ImageView view, String imageURL) {
        Glide.with(view)
                .load(imageURL)
                .error(R.drawable.background_circle)
                .circleCrop()
                .into(view);
    }

    @BindingAdapter("app:firstLetter")
    public static void firstLetter(TextView view, String text) {
        if (text != null && !text.isEmpty()) {
            view.setText(String.valueOf(text.charAt(0)));
        }
    }
}
