package com.example.projectslistapp.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.projectslistapp.R;
import com.example.projectslistapp.databinding.DialogEditNameBinding;
import com.example.projectslistapp.repo.remote.response.Project;

public class EditNameDialog extends DialogFragment {

    private DialogEditNameBinding binding;

    private EditNameListener listener;
    private Project project;

    public EditNameDialog(Project project, EditNameListener listener) {
        this.listener = listener;
        this.project = project;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        binding = DialogEditNameBinding.inflate(LayoutInflater.from(requireContext()), null, false);
        binding.editTextName.setText(project.getName());

        return new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.change_project_name)
                .setView(binding.editTextName)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.save, (dialogInterface, i) -> {
                    if (listener != null) {
                        Project updatedProject = new Project(
                                project.getId(),
                                binding.editTextName.getText().toString(),
                                project.getAvatarURL()
                        );
                        listener.changeName(updatedProject);
                    }
                })
                .create();
    }

    public interface EditNameListener {
        void changeName(Project project);
    }
}
