package com.example.projectslistapp.ui.projects_list;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.projectslistapp.repo.PreferencesRepository;
import com.example.projectslistapp.repo.remote.ProjectsService;

public class ProjectsListViewModelFactory implements ViewModelProvider.Factory {

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(ProjectsListViewModel.class)) {
            return (T) new ProjectsListViewModel(ProjectsService.getApi(), new PreferencesRepository());
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}
