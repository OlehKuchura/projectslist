package com.example.projectslistapp.ui.projects_list;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.projectslistapp.repo.PreferencesRepository;
import com.example.projectslistapp.repo.remote.ProjectsApi;
import com.example.projectslistapp.repo.remote.response.Project;
import com.example.projectslistapp.repo.remote.response.ProjectsResponse;
import com.example.projectslistapp.ui.BaseViewModel;
import com.example.projectslistapp.ui.Event;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ProjectsListViewModel extends BaseViewModel {

    private final ProjectsApi api;
    private final PreferencesRepository preferencesRepo;

    public ProjectsListViewModel(ProjectsApi api, PreferencesRepository preferencesRepo) {
        this.api = api;
        this.preferencesRepo = preferencesRepo;
    }

    private final MutableLiveData<Event<Void>> logoutEvent = new MutableLiveData<>();
    public LiveData<Event<Void>> getLogoutEvent() {
        return logoutEvent;
    }

    private final MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    public LiveData<Boolean> getLoadingStatus() {
        return isLoading;
    }

    private final MutableLiveData<List<Project>> projects = new MutableLiveData<>();
    public LiveData<List<Project>> getProjects() {
        return projects;
    }

    public void initLoadProjects() {
        if (disposable != null) return;
        api.getProjects()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ProjectsResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable = d;
                        isLoading.setValue(true);
                    }

                    @Override
                    public void onSuccess(@NonNull ProjectsResponse loadedProjects) {
                        isLoading.setValue(false);
                        projects.setValue(loadedProjects.getProjects());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLoading.setValue(false);
                    }
                });
    }

    public void changeProjectName(Project project) {
        api.updateProjectName(project.getId(), project.getName())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable = d;
                        isLoading.setValue(true);
                    }

                    @Override
                    public void onComplete() {
                        isLoading.setValue(false);
                        List<Project> newList = new ArrayList<>();
                        List<Project> oldList = projects.getValue();
                        if (oldList != null) {
                            for (Project p : oldList) {
                                if (p.getId() == project.getId()) {
                                    newList.add(project);
                                } else {
                                    newList.add(p);
                                }
                            }
                        }
                        projects.setValue(newList);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLoading.setValue(false);
                    }
                });
    }

    public void onLogoutClicked() {
        api.logout().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable = d;
                        isLoading.setValue(true);
                    }

                    @Override
                    public void onComplete() {
                        isLoading.setValue(false);
                        preferencesRepo.clearToken();
                        logoutEvent.setValue(new Event<>());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLoading.setValue(false);
                        preferencesRepo.clearToken();
                        logoutEvent.setValue(new Event<>());
                    }
                });
    }
}
