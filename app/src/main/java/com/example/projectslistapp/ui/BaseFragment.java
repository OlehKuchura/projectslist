package com.example.projectslistapp.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment<T extends ViewDataBinding> extends Fragment {

    protected T binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        binding = bindLayout(inflater, container);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    protected abstract T bindLayout(LayoutInflater inflater, ViewGroup parent);

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding.unbind();
    }

    protected void hideSoftKeyboard() {
        View focusedView = requireActivity().getCurrentFocus();
        if (focusedView == null) return;
        InputMethodManager systemService =
                (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        systemService.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
