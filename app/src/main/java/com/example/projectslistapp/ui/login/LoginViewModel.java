package com.example.projectslistapp.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.projectslistapp.repo.PreferencesRepository;
import com.example.projectslistapp.repo.remote.ProjectsApi;
import com.example.projectslistapp.repo.remote.response.LoginResponse;
import com.example.projectslistapp.ui.BaseViewModel;
import com.example.projectslistapp.ui.Event;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends BaseViewModel {

    private final ProjectsApi api;
    private final PreferencesRepository preferencesRepo;

    public LoginViewModel(ProjectsApi api, PreferencesRepository preferencesRepo) {
        this.api = api;
        this.preferencesRepo = preferencesRepo;
    }

    private final MutableLiveData<Event<Void>> openProjectsEvent = new MutableLiveData<>();
    public LiveData<Event<Void>> getOpenProjectsEvent() {
        return openProjectsEvent;
    }

    private final MutableLiveData<Event<Void>> loginErrorEvent = new MutableLiveData<>();
    public LiveData<Event<Void>> getLoginErrorEvent() {
        return loginErrorEvent;
    }

    public void checkToken() {
        if (!preferencesRepo.getToken().isEmpty()) {
            openProjectsEvent.setValue(new Event<>());
        }
    }

    public void onLoginClicked(String email, String password) {
        api.login(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<LoginResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onSuccess(@NonNull LoginResponse response) {
                        saveToken(response.getToken());
                        openProjectsEvent.setValue(new Event<>());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        loginErrorEvent.setValue(new Event<>());
                    }
                });
    }

    private void saveToken(String token) {
        preferencesRepo.saveToken(token);
    }

}
