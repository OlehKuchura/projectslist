package com.example.projectslistapp.ui.projects_list;

import androidx.recyclerview.widget.DiffUtil;

import com.example.projectslistapp.repo.remote.response.Project;

import java.util.List;

public class ProjectsDiffUtilCallback extends DiffUtil.Callback {

    private final List<Project> oldList;
    private final List<Project> newList;

    public ProjectsDiffUtilCallback(List<Project> oldList, List<Project> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Project oldProject = oldList.get(oldItemPosition);
        Project newProject = newList.get(newItemPosition);
        return oldProject.getId() == newProject.getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Project oldProject = oldList.get(oldItemPosition);
        Project newProject = newList.get(newItemPosition);
        return oldProject.compareTo(newProject) == 0;
    }
}
