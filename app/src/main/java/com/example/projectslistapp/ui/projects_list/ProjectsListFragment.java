package com.example.projectslistapp.ui.projects_list;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.projectslistapp.R;
import com.example.projectslistapp.databinding.FragmentProjectsListBinding;
import com.example.projectslistapp.repo.remote.response.Project;
import com.example.projectslistapp.ui.BaseFragment;
import com.example.projectslistapp.ui.EditNameDialog;
import com.example.projectslistapp.ui.Event;

public class ProjectsListFragment extends BaseFragment<FragmentProjectsListBinding> implements ProjectsListAdapter.OnProjectClickListener,
        EditNameDialog.EditNameListener{

    private ProjectsListViewModel viewModel;
    private final ProjectsListAdapter adapter = new ProjectsListAdapter(this);

    @Override
    protected FragmentProjectsListBinding bindLayout(LayoutInflater inflater, ViewGroup parent) {
        return FragmentProjectsListBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
        setupViewModel();
        observeProjects();
        observeLogoutEvent();
        hideSoftKeyboard();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_projects, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            viewModel.onLogoutClicked();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProjectClicked(Project project) {
        new EditNameDialog(project, this)
                .show(getChildFragmentManager(), EditNameDialog.class.getSimpleName());
    }

    @Override
    public void changeName(Project project) {
        viewModel.changeProjectName(project);
    }

    private void setupUI() {
        binding.projectsRecycler.setAdapter(adapter);
    }

    private void setupViewModel() {
        viewModel = new ViewModelProvider(requireActivity(), new ProjectsListViewModelFactory())
                .get(ProjectsListViewModel.class);
        binding.setViewModel(viewModel);
        binding.executePendingBindings();
        viewModel.initLoadProjects();
    }

    private void observeProjects() {
        viewModel.getProjects().observe(getViewLifecycleOwner(), adapter::updateProjects);
    }

    private void observeLogoutEvent() {
        viewModel.getLogoutEvent().observe(getViewLifecycleOwner(),
                new Event.EventObserver<>(content -> navigateBack()));
    }

    private void navigateBack() {
        NavHostFragment.findNavController(this).navigate(
                ProjectsListFragmentDirections.actionProjectsListFragmentToLoginFragment()
        );
    }
}