package com.example.projectslistapp.ui;

import androidx.lifecycle.Observer;

public class Event<T> {

    private T content;
    private boolean handled = false;

    public Event() {
        this.content = null;
    }

    public Event(T content) {
        this.content = content;
    }

    public boolean hasBeenHandled() {
        return handled;
    }

    public T getContentIfNotHandledOrNull() {
        if (handled) {
             return null;
        } else {
            handled = true;
            return content;
        }
    }

    public static class EventObserver<T> implements Observer<Event<T>> {
        private final ContentHandler<T> handler;

        public EventObserver(ContentHandler<T> handler) {
            this.handler = handler;
        }

        @Override
        public void onChanged(Event<T> event) {
            if (event != null) {
                if (!event.hasBeenHandled()) {
                    T content = event.getContentIfNotHandledOrNull();
                    handler.handleContent(content);
                }
            }
        }
    }

    public interface ContentHandler<T> {
        void handleContent(T Content);
    }

}
