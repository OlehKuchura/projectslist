package com.example.projectslistapp.ui.projects_list;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectslistapp.databinding.ItemProjectBinding;
import com.example.projectslistapp.repo.remote.response.Project;

import java.util.ArrayList;
import java.util.List;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

public class ProjectsListAdapter extends RecyclerView.Adapter<ProjectsListAdapter.ProjectVH> {

    private final List<Project> projects = new ArrayList<>();

    private final OnProjectClickListener listener;

    public ProjectsListAdapter(OnProjectClickListener listener) {
        this.listener = listener;
    }

    public void updateProjects(List<Project> newProjects) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new ProjectsDiffUtilCallback(projects, newProjects));
        diffResult.dispatchUpdatesTo(this);
        projects.clear();
        projects.addAll(newProjects);
    }

    @NonNull
    @Override
    public ProjectVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemProjectBinding binding = ItemProjectBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ProjectVH(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectVH holder, int position) {
        holder.bindProject(projects.get(position));
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }

    class ProjectVH extends RecyclerView.ViewHolder {

        private final ItemProjectBinding binding;

        public ProjectVH(@NonNull ItemProjectBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.projectContainer.setOnClickListener(view -> {
                int position = getAdapterPosition();
                if (position != NO_POSITION) {
                    listener.onProjectClicked(projects.get(position));
                }
            });
        }

        public void bindProject(Project project) {
            binding.setProject(project);
            binding.executePendingBindings();
        }
    }

    interface OnProjectClickListener {
        void onProjectClicked(Project project);
    }
}
