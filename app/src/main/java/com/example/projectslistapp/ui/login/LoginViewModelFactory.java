package com.example.projectslistapp.ui.login;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.projectslistapp.repo.PreferencesRepository;
import com.example.projectslistapp.repo.remote.ProjectsService;

public class LoginViewModelFactory implements ViewModelProvider.Factory {

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(ProjectsService.getApi(), new PreferencesRepository());
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}
