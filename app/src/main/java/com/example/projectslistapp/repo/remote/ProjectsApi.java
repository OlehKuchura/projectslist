package com.example.projectslistapp.repo.remote;

import com.example.projectslistapp.repo.remote.response.LoginResponse;
import com.example.projectslistapp.repo.remote.response.ProjectsResponse;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ProjectsApi {

    @FormUrlEncoded
    @POST("auth/login")
    Single<LoginResponse> login(@Field("email") String email, @Field("password") String password);

    @POST("auth/logout")
    Completable logout();

    @GET("projects-manage/index")
    Single<ProjectsResponse> getProjects();

    @FormUrlEncoded
    @POST("projects-manage/update")
    Completable updateProjectName(@Query("id") long id, @Field("name") String newName);

}
