package com.example.projectslistapp.repo.remote;

import com.example.projectslistapp.repo.PreferencesRepository;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {

    private final PreferencesRepository preferencesRepo;

    public TokenInterceptor(PreferencesRepository preferencesRepo) {
        this.preferencesRepo = preferencesRepo;
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        String token = preferencesRepo.getToken();
        if (!token.isEmpty()) {
            builder = builder.addHeader("Authorization", "Bearer " + token);
        }
        return chain.proceed(builder.build());
    }
}
