package com.example.projectslistapp.repo.remote.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProjectsResponse {

    @SerializedName("projects")
    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    private List<Project> projects;
}
