package com.example.projectslistapp.repo;

import com.orhanobut.hawk.Hawk;

public class PreferencesRepository {

     private static final String KEY_TOKEN = "token";

     public void saveToken(String token) {
         Hawk.put(KEY_TOKEN, token);
     }

     public String getToken() {
         return Hawk.get(KEY_TOKEN, "");
     }

     public void clearToken() {
         Hawk.delete(KEY_TOKEN);
     }

}
