package com.example.projectslistapp.repo.remote.response;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Project implements Comparable<Project> {

    @SerializedName("id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("logo_url")
    private String avatarURL;

    public Project() {

    }

    public Project(long id, String name, String avatarURL) {
        this.id = id;
        this.name = name;
        this.avatarURL = avatarURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int compareTo(Project compare) {
        if (compare.id == this.id
                && Objects.equals(compare.name, this.name)
                && Objects.equals(compare.avatarURL, this.avatarURL)) {
            return 0;
        } else {
            return 1;
        }
    }
}
